import { React, useEffect, useState } from 'react'

import { useTheme } from './themeContext'
import Header from './components/header'
import Filter from './components/filter'
import Country from './components/Country'
import Spin from './components/Spin'
import PerCountry from './components/PerCountry'
import {Route , Routes} from 'react-router-dom'


function App() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);
  const [cregion, setCregion] = useState("Filter by Region");
  const [subregion,setSubregion]=useState("Filter by Sub-Region");
  const [sortby, setSortBy] = useState('');
  const [sortType, setSortType] = useState('');
  const [curr,setCurr]=useState("");
  const [search,setSearch]=useState("");

  useEffect(() => {
    async function getData() {
      try {
        const response = await fetch("https://restcountries.com/v3.1/all");
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }
        const dd = await response.json();

        // console.log(dd);
        setData(dd);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setLoading(false);
      }
    }

    getData();
  }, []);
  const subRegionOption = data.reduce((acc, country) => {
    
    if (country.subregion) {
      if (!acc["Filter by Region"]) {
        acc["Filter by Region"]=  ["Filter by Sub-Region"];
      }
      if(!acc["Filter by Region"].includes(country.subregion)) {
        acc["Filter by Region"].push(country.subregion);
      }
      if (!acc[country.region]) {
        acc[country.region] = ["Filter by Sub-Region"];
      }
      if (!acc[country.region].includes(country.subregion)) {
        acc[country.region].push(country.subregion);
      }
    }
    return acc;
  }, {});

  const currencyData = data.reduce((acc, country) => {
    if (country.currencies) {
        Object.entries(country.currencies).forEach(([currCode, currData]) => {
            if (!acc[currCode]) {
                acc[currCode] = currData.name;
            }
        });
    }
    return acc;
  }, {});


  
  function handleSort(sortType, sortby) {
    setSortBy(sortby);
    setSortType(sortType);
  }


  


  const filteredCountries = data.filter(country => {
    return (
        (cregion.toLowerCase() === 'filter by region' || cregion.toLowerCase() === country.region.toLowerCase()) &&

        (subregion.toLowerCase() === 'filter by sub-region' || (country.subregion && country.subregion.toLowerCase().includes(subregion.toLowerCase()))) &&

        (country.currencies && Object.values(country.currencies)[0].name.toLowerCase().includes(curr.toLowerCase())) &&
        
        country.name.official.toLowerCase().includes(search)
    );
  });

  onSort(sortType, sortby);
  function onSort(sortType,sortby){
    if (sortby === 'population') {
      if (sortType === 'asc') {
        filteredCountries.sort((a, b) => a.population - b.population);
      } else if (sortType === 'desc') {
        filteredCountries.sort((a, b) => b.population - a.population);
      }
    }
    else if (sortby === 'area') {
      filteredCountries.sort((a, b) => {
        if (sortType === 'asc') {
          return a.area - b.area;
        } else {
          return b.area - a.area;
        }
      });
    }
  }

  const { isDarkMode} = useTheme();
  return (
    <>
      <Routes>
        <Route path='/' element={
        <section className={`container ${isDarkMode ? "dark-container" : ""}`}>
          <Header />
          <Filter searchCountry={setSearch}
            currentSelectedRegion={setCregion}
            currentSelectedSubRegion={setSubregion}
            subRegionOptions={subRegionOption}
            handleSort={handleSort}
            currData={currencyData}
            selectedCurrency={setCurr}
          />

          {loading && <Spin />}

          {loading || data && <Country data={filteredCountries}/>}
        

        </section>
        }></Route>
        <Route path="/country/:uid" element={
          
            <section className={`container ${isDarkMode ? "dark-container" : ""}`}>
              <Header/>
              <PerCountry/>
            </section>
          
        }></Route>
      </Routes>
    </>
  )
}

export default App
