import { React, useEffect, useState } from 'react';
import { useTheme } from '../themeContext';

export default function ({ searchCountry, currentSelectedRegion, currentSelectedSubRegion, subRegionOptions, handleSort ,currData,selectedCurrency}) {
  // console.log(subRegionOptions);
  // console.log(currData);
  const [isDropdownRegion, setDropdownRegion] = useState(false);
  const [isDropdownSubRegion, setDropdownSubRegion] = useState(false);
  const [isDropdownPopulation, setDropdownPopulation] = useState(false);
  const [currDown, setCurrDown] = useState(false);


  const [selectedOption, setSelectedOption] = useState("Filter by Region");

  const [selectedRegion, setSelectedRegion] = useState({ region: "Filter by Region", subRegion: "Filter by Sub-Region" });

  const [selectedSubRegion, setSelectedSubRegion] = useState("Filter by Sub-Region");
  const regionOptions = ['Filter by Region','Africa','Americas','Asia','Europe','Oceania'];



  const toggleDropdown = () => {
    setDropdownRegion(!isDropdownRegion);
  };
  const toogleDropdownSubRegion = () => {
    setDropdownSubRegion(!isDropdownSubRegion);
  }

  const toogleDropdownPopulation = () => {
    setDropdownPopulation(!isDropdownPopulation);
  }
  const handleFilterSelect = (filter) => {

    setSelectedOption(filter);
    setSelectedRegion({ region: filter, subRegion: "Filter by Sub-Region" });
    currentSelectedRegion(filter)

    setDropdownRegion(false);
  };



  const handleFilterSelectSubRegion = (filter) => {


    setSelectedRegion({ ...selectedRegion, subRegion: filter });
    // console.log(filter);

    setSelectedSubRegion(filter);
    currentSelectedSubRegion(filter);

    setDropdownSubRegion(false);

  };


  const handleSortChange = (sortType, sortby) => {
    handleSort(sortType, sortby);
    setDropdownPopulation(false);
  };

  const { isDarkMode} = useTheme();
  return (
    <section className='filter'>
      <div className={`filter-div ${isDarkMode ? "filter-div-dark" : ""}`}>

      


        {/* search  */}
        <div className="search" id={`${isDarkMode ? "search-dark" : ""}`}>
          <i className="fa-solid fa-magnifying-glass"></i>
          <input type="text" placeholder='Search for a Country...' onChange={(e)=>searchCountry(e.target.value)} />
        </div>


        {/* filter by currencies */}
        <div className="dropdown">
          <div className="dropdown-icon" onClick={()=>setCurrDown(!currDown)}>
            <h2>Sort by Currency</h2>
            <i className={currDown ? "fa-solid fa-chevron-up" : "fa-solid fa-chevron-down"}></i>
          </div>

          {currDown && (
              <ul className="dropdown-menu" id='region-dropdown'>
                  {currData && Object.entries(currData).map((curr) => (
                      <li key={curr[0]} onClick={() => {selectedCurrency(curr[1]); setCurrDown(!currDown);} }>
                        {curr[1]}
                      </li>
                  ))}
              </ul>
          )}
        </div>

        {/* sort by area and population  */}
        <div className="dropdown">
          <div className="dropdown-icon" onClick={toogleDropdownPopulation}>
            <h2>Sort by</h2>
            <i className={isDropdownPopulation ? "fa-solid fa-chevron-up" : "fa-solid fa-chevron-down"}></i>
          </div>
          {isDropdownPopulation && (
            <ul className="dropdown-menu">
              <li onClick={() => handleSortChange('asc', 'population')}>Population (Ascending)</li>
              <li onClick={() => handleSortChange('desc', 'population')}>Population (Descending)</li>
              <li onClick={() => handleSortChange('asc', 'area')}>Area (Ascending)</li>
              <li onClick={() => handleSortChange('desc', 'area')}>Area (Descending)</li>
            </ul>
          )}
        </div>


        {/* filter by sub region  */}
        <div className="dropdown">
          <div className="dropdown-icon" onClick={toogleDropdownSubRegion}>
            <h2>{selectedSubRegion}</h2>


            <i className={isDropdownSubRegion ? "fa-solid fa-chevron-up" : "fa-solid fa-chevron-down"}></i>
          </div>
          {isDropdownSubRegion && (
            <ul className="dropdown-menu" id='region-dropdown'>

              {subRegionOptions[selectedRegion.region] && subRegionOptions[selectedRegion.region].map(option => (
                <li key={option} onClick={() => handleFilterSelectSubRegion(option)} style={{ display: selectedRegion === option ? 'none' : 'block' }}>
                  {option}
                </li>
              ))}
            </ul>
          )}
        </div>


        {/* filter by region  */}
        <div className="dropdown">
          <div className="dropdown-icon" onClick={toggleDropdown}>
            <h2>{selectedOption}</h2>
            <i className={isDropdownRegion ? "fa-solid fa-chevron-up" : "fa-solid fa-chevron-down"}></i>
          </div>
          {isDropdownRegion && (
            <ul className="dropdown-menu">
              {regionOptions.map(option => (
                <li key={option} onClick={() => handleFilterSelect(option)} style={{ display: selectedOption === option ? 'none' : 'block' }}>
                  {option}
                </li>
              ))}
            </ul>
          )}
        </div>


      </div>
    </section>
  )
}









