import React, { useState } from 'react'
import { useTheme } from '../themeContext';
import { useNavigate } from 'react-router-dom';

export default function Country({ data }) {
  // console.log(data, typeof data)
  // const [unid,setUnid]=useState(null);
  const { isDarkMode} = useTheme();
  const navigate=useNavigate();


  const carddet=(uid)=>{
    navigate(`/country/${uid}`);
  }

  return (
    <section className='country-data'>
      <div className={`country ${isDarkMode ? "country-dark" : ""} `}>
        {(data.length>0) ? (Object.entries(data).map(([key, country]) => (
          <div className='display' key={key} onClick={()=>carddet(country.ccn3)}>
            <div className='img-div'>
              <img src={country.flags.png} alt="" />
            </div>
            
            <div className='details-div'>
              <div className="details">
                  <div className='official-name'> 
                    <h1>{country.name.official}</h1>
                  </div>
                  <div className='text-div'>
                    <p><span className='text'>Population: </span>{country.population}</p>
                    <p><span className='text'>Region: </span>{country.region}</p>
                    <p><span className='text'>Capital: </span>{country.capital}</p>
                    <p>
                    <span className='text'>Currencies:</span> {country.currencies &&
                            <span>
                                {Object.values(country.currencies).map(currency => currency.name).join(', ')}
                            </span>
                        }
                      </p>
                  </div>
                  
              </div>
              
            </div>
           
            
          </div>
        ))) : <div className='nothing'>No such countries found</div>}
      </div>
    </section>
  )
}
