import {React,useEffect, useState} from 'react'


import { useTheme } from '../themeContext';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';

export default function () {

    const [perdata,setPerdata]=useState({});

    const [err,setErr]=useState(true);
    const [countryname,setCountryname]=useState({});

    const { isDarkMode } = useTheme();
    const navigate =useNavigate();
    const {uid}=useParams();

    
    function back(){
        navigate('/');
    }

    
    useEffect(() => {
        async function getData() {
          try {
            const response = await fetch(`https://restcountries.com/v3.1/alpha/${uid}`);
            const response2 =  await fetch("https://restcountries.com/v3.1/all");
            if (!response.ok) {
              throw new Error('Failed to fetch data');
            }

            const alldata=await response2.json();
            const perCData = await response.json();
            setPerdata(perCData[0]);

            // console.log(perCData,typeof(perCData), perCData.flags, perCData[0].flags)

            setCountryname(alldata.reduce((acc,curr)=>{
                const cname=curr.cca3;
                if(!acc[cname]){
                  acc[cname]=[];
                }
                acc[cname].push(curr.name.common);
                return acc;
            },{}));

          }
          catch (error) {
            console.error('Error fetching data:', error);
            // setLoading(false);
            setErr(false);
          }
        }
    
        getData();
      }, [uid]);
    // console.log(perdata);
    return (

        <>
        {err && perdata &&
        <div className='per-country'>
            <div className={`btn-div ${isDarkMode ?"btn-div-dark" : ""}`}onClick={back}>
                <i className="fa-solid fa-arrow-left-long"></i>
                <p>Back</p>
            </div>
            <div className='country-detail'>
                <div className='c-img-div'>
                    {perdata.flags && <img src={perdata.flags.png} alt="" />}
                </div>

                <div className='c-div-details'>
                    {perdata.name &&<h2>{perdata.name.common}</h2>}

                    <div className={`extra-details ${isDarkMode ? "extra-details-dark" :""}`}>
                      <div className='extra-details-left'>
                        <p>Native Name: {perdata.altSpellings &&<span>{perdata.altSpellings[1]}</span>}</p>

                        <p>Population : {perdata.population &&<span>{perdata.population.toLocaleString()}</span>}</p>

                        <p>Region : {perdata.region&&<span>{perdata.region}</span>}</p>

                        <p>Sub Region: {perdata.subregion&&<span>{perdata.subregion}</span>}</p>

                        <p>Capital : {perdata.capital&&<span>{perdata.capital}</span>}</p>
                      </div>
                      <div className='extra-details-right'>
                        <p>Top Level Domain : {perdata.tld&&<span>{perdata.tld}</span>}</p>
                        {/* <p>Currencies : {perdata.currencies&&<span>{perdata.currencies[0]}</span>}</p> */}
                        <p>
                            Currencies: {perdata.currencies &&
                                <span>
                                    {Object.values(perdata.currencies).map(currency => currency.name).join(', ')}
                                </span>
                            }
                        </p>
                        <p>
                          Languages : {perdata.languages && (
                          <span>
                              {Object.entries(perdata.languages).map(([key, value]) => (
                                  <span className='border' key={key}>
                                      {value}
                                      {key !== Object.keys(perdata.languages)[Object.keys(perdata.languages).length - 1] && ", "}
                                  </span>
                              ))}
                          </span>
                          )}
                      </p>

                      </div>
                    </div>


                    <div className='border-div'>
                        <p className='border-text'> Border Countries :</p>
                        <span className='border-span'>
                        {
                          perdata.borders ? perdata.borders.map((border,index) => (
                            (<span className={`btn-div ${isDarkMode ?"btn-div-dark" : ""}`} key={index}>
                              {countryname[border]}
                            </span>)
                          ))
                          :
                          (<strong>No border Countries</strong>)
                        }
                        </span>
                    </div>
                </div>
            </div>
        </div>
        }
        </>
    )
}
