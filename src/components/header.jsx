import React from 'react'
import { useTheme } from '../themeContext';


export default function () {

  const { isDarkMode, toggleTheme } = useTheme();

  return (
    <section className={`header ${isDarkMode ? "header-dark" : ""} `}>
        <div className='header-div' >
            <h1>Where in the world?</h1>
            <p className='dark-mode' onClick={toggleTheme}><i className="fa-regular fa-moon"></i>Dark Mode</p>
        </div>
    </section>
  )
}
